SHELL=/bin/bash
all: clean sdk publish
##服务
PACKAGES=passport helloworld citadel
#sdk输出目录
SDK_OUTPUT=./_publish
CPPFLAGS += -I.
# CPPFLAGS += -I$$GOPATH/src
# CPPFLAGS += -I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis

lint:
	prototool lint

sdk: init-sdk go-sdk php-sdk python-sdk java-sdk

publish: publish-go publish-py publish-php

clean:
	@rm -rf $(SDK_OUTPUT)
	@rm -rf _tmp

init-sdk:
	@mkdir -p $(SDK_OUTPUT)

go-sdk:
	@for package in $(PACKAGES); do\
		echo doing generate $${package} go sdk  ... &&\
		protoc $(CPPFLAGS) --go_out=plugins=grpc+ymtgrpc:$(SDK_OUTPUT) --go-grpc_out=$(SDK_OUTPUT) --plugin=protoc-gen-grpc_go=`which protoc-gen-go` --grpc-gateway_out=logtostderr=true:$(SDK_OUTPUT) $${package}/*.proto ; \
	done

php-sdk:
	@for package in $(PACKAGES); do\
		echo doing generate $${package} php sdk  ... &&\
		protoc $(CPPFLAGS) --php_out=$(SDK_OUTPUT)/$${package} --plugin=protoc-gen-grpc_php=`which grpc_php_plugin` --grpc_php_out=$(SDK_OUTPUT)/$${package} $${package}/*.proto ;\
	done

python-sdk:
	@for package in $(PACKAGES); do\
		echo doing generate $${package} python sdk  ... &&\
		protoc $(CPPFLAGS) --python_out=$(SDK_OUTPUT) --plugin=protoc-gen-grpc_python=`which grpc_python_plugin` --grpc_python_out=$(SDK_OUTPUT) $${package}/*.proto  &&\
		touch $(SDK_OUTPUT)/$${package}/__init__.py ;\
	done

java-sdk:
	@mkdir -p $(SDK_OUTPUT)/src/main/java/
	@for package in $(PACKAGES); do\
		echo doing generate $${package} python sdk  ... &&\
		protoc $(CPPFLAGS) --java_out=$(SDK_OUTPUT)/src/main/java/ --plugin=protoc-gen-grpc_java=`which grpc_java_plugin` --grpc_java_out=$(SDK_OUTPUT)/src/main/java/ $${package}/*.proto  ;\
	done

publish-go:
	@cp export/go.mod $(SDK_OUTPUT)/
	@echo "package protos" >$(SDK_OUTPUT)/protos.go

publish-py:
	@cp export/setup.py $(SDK_OUTPUT)/

publish-java:
	@cp export/pom.xml $(SDK_OUTPUT)/
	@
	@cd $(SDK_OUTPUT) && mvn deploy

publish-php:
	@cp export/composer.json $(SDK_OUTPUT)/composer.json
	@for package in $(PACKAGES); do\
		upper_package="$$(tr '[:lower:]' '[:upper:]' <<< $${package:0:1})$${package:1}" &&\
		jq -r '.|.autoload."psr-4"=.autoload."psr-4"+{"'$${upper_package}'\\":"'$${package}'/'$${upper_package}'"}' $(SDK_OUTPUT)/composer.json > _tmp.json  &&\
		mv _tmp.json $(SDK_OUTPUT)/composer.json &&\
		jq -r '.|.autoload."psr-4"=.autoload."psr-4"+{"GPBMetadata\\'$${upper_package}'\\":"'$${package}'/GPBMetadata/'$${upper_package}'"}' $(SDK_OUTPUT)/composer.json > _tmp.json  &&\
		mv _tmp.json $(SDK_OUTPUT)/composer.json ;\
	done

copy-to-branch:
	mkdir -p _tmp
	cp -r .git _tmp/
	if [[ ${CI_COMMIT_REF_SLUG} =~ "-source" ]]; then \
		echo "copy sdk to $(subst -source,,${CI_COMMIT_REF_SLUG}) branch ..." ;\
	else \
		echo 仅支持以-source结尾的分支名的sdk发布 &&\
		exit 1;\
	fi

	branch=$(subst -source,,${CI_COMMIT_REF_SLUG}) &&\
	remote=`git ls-remote --heads origin $$branch` &&\
	if [ ! "$$remote" ]; then \
		cd _tmp && git checkout -b $${branch} ;\
	else \
		cd _tmp && git pull origin $${branch} && git checkout $${branch} &&\
		find . -type 'd' | grep -v ".git" | xargs rmdir &&\
		find . -type 'f' | grep -v ".git" | xargs rm &&\
		ls -alh;\
	fi
	ssh-keyscan ${CI_SERVER_HOST} >> ~/.ssh/known_hosts
	cp -r ${SDK_OUTPUT}/* _tmp/
	cd _tmp && git remote add ssh git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git
	echo $$CI_COMMIT_SHA > _tmp/lastcommit_sha
	branch=$(subst -source,,${CI_COMMIT_REF_SLUG}) &&\
	cd _tmp && git add . && git commit -m "$$CI_COMMIT_SHA" && git push ssh $${branch}
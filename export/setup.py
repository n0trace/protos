#! /usr/bin/env python
# -*- coding: utf-8 -*-
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
import setuptools

setup(
    name='protos',
    author='wangyufeng',
    version='0.1.0',
    license='MIT',

    description='grpc sdk for python',
    long_description='''long description''',
    author_email='wangyufeng@ymt360.com',
    url='git.ymt360.com/wangyufeng/protos',
    packages=find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    install_requires=[
        'grpcio',
        'googleapis-common-protos'
    ],
    dependency_links=[
        'git+https://git.ymt360.com/common/ymtgrpc.git@develop'
    ],
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries'
    ],
    zip_safe=True,
)
